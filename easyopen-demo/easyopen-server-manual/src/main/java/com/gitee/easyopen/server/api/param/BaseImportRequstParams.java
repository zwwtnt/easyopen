package com.gitee.easyopen.server.api.param;

import com.gitee.easyopen.doc.DataType;
import com.gitee.easyopen.doc.annotation.ApiDocField;
import org.springframework.web.multipart.MultipartFile;

public class BaseImportRequstParams {
    @ApiDocField(description = "Excel文件", required = false, dataType = DataType.FILE)
    MultipartFile file;//上传文件

    private String traceLogId;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getTraceLogId() {
        return traceLogId;
    }

    public void setTraceLogId(String traceLogId) {
        this.traceLogId = traceLogId;
    }
}